//
//  ProductCatalogViewController.m
//  KDS
//
//  Created by Tiseno Mac 2 on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ProductCatalogViewController.h"

//@interface ProductCatalogViewController ()

//@end

@implementation ProductCatalogViewController
@synthesize itemScrollView,productsArr,productsArr_Copied,category;
@synthesize currentSelectedCustomer;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.currentSelectedCustomer = ((KDSAppDelegate*)[UIApplication sharedApplication].delegate).currentCustomer;
    }
    return self;
}

-(void)initializeCategoryButtons:(NSArray*) itemArr
{
    //NSLog(@"%@",itemArr);
    int widthOffset = 0;
    int yOffset=0;
    int xOffset=2;
    for(int i=0;i<itemArr.count;i++)
    {
        
        if(i!=0 && (i%2)==0)
        {
            yOffset=0;
            xOffset+=150;
        }
        Item* item=[itemArr objectAtIndex:i];
        ButtonWithProduct *categoryButton_up = [[[ButtonWithProduct alloc] init] autorelease];
        if(item.Image_Path!=nil && ![item.Image_Path isEqualToString:@""])
        {
            NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString* documentsPath = [paths objectAtIndex:0];
            NSString* filePath = [documentsPath stringByAppendingPathComponent:item.Image_Path];
            NSData* pngData = [NSData dataWithContentsOfFile:filePath];
            UIImage* image = [UIImage imageWithData:pngData];
            
            [categoryButton_up setBackgroundImage:image forState:UIControlStateNormal];
            //NSLog(@"image loaded");
        }
        categoryButton_up.item = item;
        //categoryButton_up.frame = CGRectMake(1+widthOffset*xOffset, yOffset, 137, 137);
        categoryButton_up.frame = CGRectMake(xOffset, 2+widthOffset*yOffset, 137, 137);
        [categoryButton_up addTarget:self action:@selector(productImageTapped:) forControlEvents:UIControlEventTouchDown];
        
        /**/UILabel *orderIdLabel_up = [[UILabel alloc] initWithFrame:CGRectMake(xOffset, 2+widthOffset*yOffset+137, 137, 21)];
        orderIdLabel_up.adjustsFontSizeToFitWidth = YES;
        orderIdLabel_up.font = [UIFont fontWithName:@"Helvetica-Bold" size:9];
        orderIdLabel_up.minimumFontSize=7;
        orderIdLabel_up.textAlignment=UITextAlignmentCenter;
        orderIdLabel_up.textColor = [UIColor blackColor];
        orderIdLabel_up.backgroundColor = [UIColor clearColor];
        orderIdLabel_up.text=item.Item_No;
        
        UILabel *categoryLabel_up = [[UILabel alloc] initWithFrame:CGRectMake(xOffset, 2+widthOffset*yOffset+155, 137, 21)];
        categoryLabel_up.adjustsFontSizeToFitWidth = YES;
        categoryLabel_up.minimumFontSize=7;
        categoryLabel_up.textAlignment=UITextAlignmentCenter;
        categoryLabel_up.textColor = [UIColor blackColor];
        categoryLabel_up.backgroundColor = [UIColor clearColor];
        categoryLabel_up.text=item.Description;
        widthOffset=187;
        //xOffset++;
        yOffset++;
        
        [self.itemScrollView addSubview:categoryButton_up];
        /**/[self.itemScrollView addSubview:orderIdLabel_up];
        [self.itemScrollView addSubview:categoryLabel_up];
        
        [categoryLabel_up release];
        //[orderIdLabel_up release];
        //[categoryButton_up release];
        
    }
    self.itemScrollView.contentSize=CGSizeMake(xOffset+150, self.itemScrollView.frame.size.height);
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    KDSAppDelegate* appDelegate = (KDSAppDelegate*)[UIApplication sharedApplication].delegate;
    if(appDelegate.itemCategoryDictionary == nil)
    {
        [appDelegate loadProducts];
    }
    NSArray *itemArr = [appDelegate.itemCategoryDictionary objectForKey:self.category];
    
    NSLog(@"%@",itemArr);
    
    if(productsArr == nil)
    {
        productsArr = itemArr;
    }
    if(productsArr_Copied==nil)
    {
        NSMutableArray* tProductArr_Copied = [[NSMutableArray alloc]init];
        self.productsArr_Copied = tProductArr_Copied;
        [tProductArr_Copied release];
    }
    for(int i=0;i<[self.productsArr count];i++)
    {
        [productsArr_Copied addObject:[productsArr objectAtIndex:i]];
    }
    [self initializeCategoryButtons:itemArr];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];

}

- (void)viewDidUnload
{

    //[self setItemScrollView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)dealloc 
{
    [currentSelectedCustomer release];
    [category release];
    [productsArr release];
    [productsArr_Copied release];
    [itemScrollView release];
    [super dealloc];
}

-(IBAction)productImageTapped:(id)sender
{
    NewOrder_ConfirmViewController *orderConfirmViewController=[[NewOrder_ConfirmViewController alloc] initWithNibName:@"NewOrder_ConfirmViewController" bundle:nil];
    orderConfirmViewController.currentSelectedItem = ((ButtonWithProduct*)sender).item;
    orderConfirmViewController.title=@"iOrder";
    [self.navigationController pushViewController:orderConfirmViewController animated:YES];
    [orderConfirmViewController release];
}


@end
