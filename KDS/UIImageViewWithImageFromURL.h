//
//  UIImageViewWithImageFromURL.h
//  KDS
//
//  Created by Jermin Bazazian on 12/15/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageViewDelegate.h"


@interface UIImageViewWithImageFromURL : UIImageView<AsyncImageViewDelegate> {
    
}

@end
