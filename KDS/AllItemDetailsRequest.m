//
//  AllItemDetailsRequest.m
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/6/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "AllItemDetailsRequest.h"


@implementation AllItemDetailsRequest
-(id)init
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"GetAllItemDetails";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{
    return @"";
}
@end
