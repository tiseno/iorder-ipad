//
//  DownloadImages.m
//  KDS
//
//  Created by Tiseno Mac 2 on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DownloadImages.h"

@implementation DownloadImages
@synthesize Image_Path;

-(void)handleRecieveImage:(UIImage *)image sender:(AsyncImageView *)sender
{
    if(image != nil)
    {
        NSData* pngData = UIImagePNGRepresentation(image);
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsPath = [paths objectAtIndex:0];
        NSString* filePath = [documentsPath stringByAppendingPathComponent:self.Image_Path];
        [pngData writeToFile:filePath atomically:YES];
        NSLog(@"Image Saved");
    }
    
}

@end
