//
//  BlindItem.h
//  KDS
//
//  Created by Tiseno on 1/9/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"


@interface BlindItem : Item {

    float min_Height;
    float min_SqFt;
    float total_SqFt;
}
@property(nonatomic) float min_Height;
@property(nonatomic) float min_SqFt;
@property(nonatomic) float total_SqFt;
@end
