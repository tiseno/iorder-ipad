//
//  OrderHistoryTableCellViewController.h
//  KDS
//
//  Created by Tiseno on 12/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface OrderHistoryTableCellViewController : UITableViewCell {
    
    UIColor *lineColor;
    
    UILabel *orderDateLabel;
    UILabel *orderPriceLabel;
    UILabel *orderIdPrefix;
}
@property (nonatomic,retain) UIColor *lineColor;
@property (nonatomic,retain) UILabel *orderDateLabel;
@property (nonatomic,retain) UILabel *orderPriceLabel;
@property (nonatomic,retain) UILabel *orderIdPrefix;
@end
