//
//  DeletedCartViewController.h
//  KDS
//
//  Created by Tiseno Mac 2 on 5/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderListViewController.h"
#import "GridDeletedTableView.h"
#import "SalesOrderItem.h"
#import "UITableViewLoadingDelegate.h"
#import "KDSAppDelegate.h"
#import "SalesOrder.h"
#import "NewOrderCategoryViewController.h"
#import "ChangeDiscountTypeButtonDelegate.h"
//#import "YourCartTableCellDelegate.h"
#import "KDSDataSalesOrder.h"
#import "KDSDataBlindSalesOrder.h"
#import "MenuViewController.h"
#import "MenuViewController.h"
#import "NewOrderCategoryViewController.h"
#import "UITableViewWithLoadingNotification.h"
#import "TransferedReviewTableViewController.h"
#import "EditCartView.h"
#import "UpdateSelectedCartView.h"
#import "EditCartController.h"
#import "NewOrderUpdateConfirmView.h"

#define kConfirmingAlertTag 0
#define kConfirmedAlertTag 1
#define kCancelAlertTag 2
#define kUnsavedAlertTag 3


@class UITableView;
@interface DeletedCartViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate, UITableViewLoadingDelegate, ReviewTableViewDelegate, UpdateSelectedCartView> {
    
    int numberOfCellsGenerated;
    NSArray *orderItemsArr;
    SalesOrderItem* orderItem;
    float totalDiscountOfItems;
    
    UITableViewWithLoadingNotification *_tableView;
    UIView *tableViewHeader;
    UIView *reviewUIView;
    UIView *UpdateCartUIView;
    
    UILabel *installationNameTextField;
    UILabel *cityTextField;
    UILabel *stateTextField;
    UILabel *dateTextField;
    UILabel *timeTextField;
    UILabel *telTextField;
    UILabel *faxTextField;
    UILabel *emailTextField;
    UILabel *transportationTextField;
    UILabel *installationTextField;
    UILabel *totalDiscountTextField;
    
    UIButton *backButton;
    SalesOrder* CurrentSaleOrder;
    TransferedReviewTableViewController* reviewTableViewController;
    
    UILabel *grandTotalLabel;
    UILabel *totalPriceLabel;
    UILabel *orderIdLabel;
    UILabel *orderIdValueLabel;
    
    UITableView *reviewTableView;
    UIView *EditCartSmallView;
    
    UIImageView* backgroundImageView;
    
    UILabel *installationAddressTextView;
    
    UILabel *remarkTextView;
    UITextView *alertUser;
    
    int numberOfRows;
    int heightOfEditedArea;
    int heightOffset;
    BOOL iskeyboardDisplayed;
    BOOL isTableLoaded;
    BOOL isDiscountAmount;
    float totalPricce;
    float grandPrice;
    float discountOnTotalPrice;
    int numberOfBlindOrders;
    BOOL shouldUpdateCart;
    SalesOrderItem* salesOrderItem;
    NSMutableArray *allsalesItem;
    
    NSString *UpdaCartPrice;
    int UpdateOrderItemIndex;
    NSMutableDictionary *itemCategoryDictionary;
    
}
//-(void)fillItemCateogryDictionaryWithItemArr:(NSArray*)itemarr;
@property (nonatomic, retain) NSMutableDictionary *itemCategoryDictionary;
@property (nonatomic,retain) NSArray *orderItemsArr;
@property (nonatomic,retain) SalesOrderItem* orderItem;
@property (nonatomic) float totalDiscountOfItems;
//@property (retain, nonatomic) IBOutlet UITableView *_tableView;
@property (nonatomic, retain) IBOutlet UITableViewWithLoadingNotification *_tableView;
@property (retain, nonatomic) IBOutlet UILabel *remarkTextView;
@property (retain, nonatomic) IBOutlet UILabel *DeliveryDateTextField;

//-(IBAction)orderListButtonTapped;

@property (nonatomic, retain) UIButton *backButton;
@property (nonatomic, retain) UITableView *reviewTableView;
@property (nonatomic, retain) UIView *EditCartSmallView;
@property (nonatomic, retain) UIView *reviewUIView;
@property (nonatomic, retain) UIView *UpdateCartUIView;

@property (nonatomic) float discountOnTotalPrice;
@property (nonatomic, retain) UIView *tableViewHeader;
@property (nonatomic, retain) SalesOrder* CurrentSaleOrder;
@property (nonatomic, retain) TransferedReviewTableViewController* reviewTableViewController;
//@property (nonatomic, retain) UpdateSelectedCartView* UpdateSelectedCartViewset;
@property (nonatomic, retain) EditCartView* editCartViewcoltroller;
@property (nonatomic,retain) EditCartController* editCartController;
@property (nonatomic, retain) IBOutlet UIImageView* backgroundImageView;
@property (nonatomic, retain) IBOutlet UILabel *installationNameTextField;
@property (nonatomic, retain) IBOutlet UILabel *cityTextField;
@property (nonatomic, retain) IBOutlet UILabel *stateTextField;
@property (nonatomic, retain) IBOutlet UILabel *dateTextField;
@property (nonatomic, retain) IBOutlet UILabel *timeTextField;
@property (nonatomic, retain) IBOutlet UILabel *telTextField;
@property (nonatomic, retain) IBOutlet UILabel *faxTextField;
@property (nonatomic, retain) IBOutlet UILabel *emailTextField;
@property (nonatomic, retain) IBOutlet UILabel *transportationTextField;
@property (nonatomic, retain) IBOutlet UILabel *installationTextField;
@property (nonatomic, retain) IBOutlet UILabel *totalDiscountTextField;
@property (nonatomic, retain) IBOutlet UILabel *grandTotalLabel;
@property (nonatomic, retain) IBOutlet UILabel *totalPriceLabel;
@property (nonatomic, retain) IBOutlet UILabel *orderIdLabel;
@property (nonatomic, retain) IBOutlet UILabel *orderIdValueLabel;
@property (nonatomic, retain) IBOutlet UITextView *alertUser;

@property (retain, nonatomic) IBOutlet UILabel *lbldeliveryDate;
@property (retain, nonatomic) IBOutlet UILabel *DescripttionTextView;
@property (retain, nonatomic) IBOutlet UILabel *lblDescription;
-(IBAction)reviewButtonTapped;
-(void)updateCart;
-(void)manageReviewView;
-(void)manageUpdateCartView;
//-(IBAction)UpdateScrollToItem:(id)sender;
@property (nonatomic, retain) NSMutableArray *allsalesItem;

@property (nonatomic,retain) UITextField *gpriceTextField;

@property (nonatomic,retain) NSString *UpdaCartPrice;
-(void)tableCell:(UITableViewCell*)cell UpdateItemAtIndex:(int)orderItemIndex;

@property (nonatomic) int UpdateOrderItemIndex;
@property (nonatomic,retain) NSArray *getsumOfOrderItemPrice;
@property (nonatomic,retain) NSString *EditTag;

@property (retain, nonatomic) IBOutlet UILabel *lbltransportation;
@property (retain, nonatomic) IBOutlet UILabel *lblinstallation;
@property (retain, nonatomic) IBOutlet UILabel *lblinstallationName;
@property (retain, nonatomic) IBOutlet UILabel *lblinstallationAddress;
@property (retain, nonatomic) IBOutlet UILabel *lbldate;
@property (retain, nonatomic) IBOutlet UILabel *lbltime;
@property (retain, nonatomic) IBOutlet UILabel *lbltel;
@property (retain, nonatomic) IBOutlet UILabel *lblfax;
@property (retain, nonatomic) IBOutlet UILabel *lblcontact;
@property (retain, nonatomic) IBOutlet UILabel *lblcustomerCompanyName;
@property (retain, nonatomic) IBOutlet UILabel *lblcustomerTel;
@property (retain, nonatomic) IBOutlet UILabel *txtcusRemark;
@property (retain, nonatomic) IBOutlet UILabel *lblcusRemark;
@property (retain, nonatomic) IBOutlet UILabel *txtorderref;
@property (retain, nonatomic) IBOutlet UILabel *installationAddressTextView;
@property (retain, nonatomic) IBOutlet UILabel *lblreference;
@property (retain, nonatomic) IBOutlet UILabel *ShippingAddress;
@property (retain, nonatomic) IBOutlet UILabel *lblshippingAddress;
@property (retain, nonatomic) IBOutlet UILabel *lblPO;
@property (retain, nonatomic) IBOutlet UILabel *txtPO;
@property (retain, nonatomic) IBOutlet UIScrollView *mainScrolView;
@property (retain, nonatomic) IBOutlet UIButton *btnordersummary;

@property (retain, nonatomic) IBOutlet UILabel *lblremarkmain;
@property (retain, nonatomic) IBOutlet UILabel *lblQtymain;
@property (retain, nonatomic) IBOutlet UILabel *lblunitPricemain;
@property (retain, nonatomic) IBOutlet UILabel *lbldiscountmain;
@property (retain, nonatomic) IBOutlet UILabel *lbltotalmain;

@property (retain, nonatomic) IBOutlet UILabel *lbldescblind;
@property (retain, nonatomic) IBOutlet UILabel *lblcontrolblind;
@property (retain, nonatomic) IBOutlet UILabel *lblqtyblind;
@property (retain, nonatomic) IBOutlet UILabel *lblsqftblind;
@property (retain, nonatomic) IBOutlet UILabel *lbltotalsqftblind;
@property (retain, nonatomic) IBOutlet UILabel *lblunitpriceblind;
@property (retain, nonatomic) IBOutlet UILabel *lbldiscountblind;
@property (retain, nonatomic) IBOutlet UILabel *lbltotalblind;

@property (retain, nonatomic) IBOutlet UILabel *lbldeliverydateblind;
@property (retain, nonatomic) IBOutlet UILabel *txtdeliveryDateblind;




@end




