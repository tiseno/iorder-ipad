//
//  ReviewTableViewCell.m
//  KDS
//
//  Created by Tiseno on 1/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ReviewTableViewCell.h"


@implementation ReviewTableViewCell
@synthesize itemIdLabel, itemNameLabel, itemWidthDrop, itemSqrFeet, itemQuantityLabel, orderPriceLabel, totalPriceLabel,orderDiscount;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 740, 35);
        
        /*UILabel *tItemIdLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, 100, self.frame.size.height)];
         tItemIdLabel.textAlignment = UITextAlignmentCenter;
         tItemIdLabel.backgroundColor = [UIColor clearColor];
         tItemIdLabel.textColor = [UIColor blackColor];
         self.itemIdLabel = tItemIdLabel;
         [tItemIdLabel release];
         [self addSubview:itemIdLabel];*/
        
        UILabel *tItemNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.origin.y, 350, self.frame.size.height)];
        tItemNameLabel.textAlignment = UITextAlignmentCenter;
        tItemNameLabel.backgroundColor = [UIColor clearColor];
        tItemNameLabel.textColor = [UIColor blackColor];
        self.itemNameLabel = tItemNameLabel;
        [tItemNameLabel release];
        [self addSubview:itemNameLabel];
        
        UILabel *tItemQuantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(360, self.frame.origin.y, 410, self.frame.size.height)];
        tItemQuantityLabel.textAlignment = UITextAlignmentCenter;
        tItemQuantityLabel.backgroundColor = [UIColor clearColor];
        tItemQuantityLabel.textColor = [UIColor blackColor];
        self.itemQuantityLabel = tItemQuantityLabel;
        [tItemQuantityLabel release];
        [self addSubview:itemQuantityLabel];
        
        UILabel *tOrderPrice = [[UILabel alloc] initWithFrame:CGRectMake(420, self.frame.origin.y, 470, self.frame.size.height)];
        tOrderPrice.textAlignment = UITextAlignmentCenter;
        tOrderPrice.backgroundColor = [UIColor clearColor];
        tOrderPrice.textColor = [UIColor blackColor];
        self.orderPriceLabel = tOrderPrice;
        [tOrderPrice release];
        [self addSubview:orderPriceLabel];
        
        UILabel *tOrderDiscount = [[UILabel alloc] initWithFrame:CGRectMake(480, self.frame.origin.y, 530, self.frame.size.height)];
        tOrderDiscount.textAlignment = UITextAlignmentCenter;
        tOrderDiscount.backgroundColor = [UIColor clearColor];
        tOrderDiscount.textColor = [UIColor blackColor];
        self.orderDiscount = tOrderDiscount;
        [tOrderDiscount release];
        [self addSubview:orderDiscount];
        
        UILabel *tTotalPrice = [[UILabel alloc] initWithFrame:CGRectMake(540, self.frame.origin.y, 590, self.frame.size.height)];
        tTotalPrice.textAlignment = UITextAlignmentCenter;
        tTotalPrice.backgroundColor = [UIColor clearColor];
        tTotalPrice.textColor = [UIColor blackColor];
        self.totalPriceLabel = tTotalPrice;
        [tTotalPrice release];
        [self addSubview:totalPriceLabel];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [orderPriceLabel release];
    [itemQuantityLabel release];
    [itemNameLabel release];
    [itemIdLabel release];
    [super dealloc];
}

@end
