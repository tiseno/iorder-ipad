//
//  ReviewTableViewController.m
//  KDS
//
//  Created by Tiseno on 1/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ReviewTableViewController.h"


@implementation ReviewTableViewController
@synthesize numberOfRows, scrollDelegate;
- (id)init
{
    if (self) {
        // Initialization code
    }
    return self;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}
-(NSInteger) tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    if(appDelegate.salesOrder!=nil)
    {
        NSArray* orderItemsArr=[appDelegate.salesOrder order_Items];
        numberOfRows = orderItemsArr.count;
        return numberOfRows;
    }
    return 0;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    //ReviewTableViewCell *cell =(ReviewTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    NSArray* orderItemsArr=[appDelegate.salesOrder order_Items];
    
    SalesOrderItem* orderItem=[orderItemsArr objectAtIndex:indexPath.row];
    float discCheck,totalDisc;
    
    
    
    if((![orderItem.item.Category isEqualToString:kBlindCategory]) && (![orderItem.item.Category isEqualToString:kblindACategoryA]))
    {
        NSLog(@"main orderItem.item.Category--->%@",orderItem.item.Category);
        
        ReviewTableViewCell *cell =(ReviewTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(cell == nil)
        {
            /*float total = (orderItem.Unit_Price*orderItem.Quantity)-(discCheck*(orderItem.Unit_Price*orderItem.Quantity));
            if(orderItem.Discount_Amount==0)
            {
                discCheck = orderItem.Discount_Percent;
                totalDisc = (discCheck/100)*total;
            }
            else
            {
                discCheck = orderItem.Discount_Amount; 
                totalDisc = discCheck;
            }*/
            NSString* discountStr;
            float total = (orderItem.Unit_Price*orderItem.Quantity);
            if(orderItem.Discount_Amount==0)
            {
                discCheck = orderItem.Discount_Percent;
                totalDisc = (discCheck/100)*total;                
                total = total - totalDisc;
                discountStr=[NSString stringWithFormat:@"%.2f %%",discCheck];
            }
            else if(orderItem.Discount_Percent==0)
            {
                discCheck = orderItem.Discount_Amount; 
                totalDisc = discCheck;
                total = total - totalDisc;
                discountStr=[NSString stringWithFormat:@"RM %.2f",discCheck];
            }else 
            {
                total = totalDisc;
            }
            
            
            cell = [[[ReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.itemIdLabel.text = orderItem.item.Item_No;
            cell.itemNameLabel.text = orderItem.item.Description;
            
            NSString* quantityStr=[NSString stringWithFormat:@"%.0f",orderItem.Quantity];
            cell.itemQuantityLabel.text=quantityStr;
            
            NSString* totalStr=[NSString stringWithFormat:@"%.2f",total];
            cell.totalPriceLabel.text=totalStr;
            
            
            cell.orderDiscount.text=discountStr;
            
            NSString* priceStr=[NSString stringWithFormat:@"%.2f",orderItem.Unit_Price];
            cell.orderPriceLabel.text=priceStr;
        }
        return cell;
    }
    else //if([orderItem.item.Category isEqualToString:kBlindCategory] && [orderItem.item.Category isEqualToString:kblindACategoryA])
    {
        //NSLog(@"blind orderItem.item.Category--->%@",orderItem.item.Category);
        ReviewTableViewCellBlind *cell =(ReviewTableViewCellBlind*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if(cell == nil)
        {
            
            
            cell = [[[ReviewTableViewCellBlind alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.itemIdLabel.text = orderItem.item.Item_No;
            cell.itemNameLabel.text = orderItem.item.Description;
            
            
            if([orderItem.item.Category isEqualToString:kBlindCategory]) 
            {
                NSString* discountStr;
                
                NSString *orderItemUnit_Price=[NSString stringWithFormat:@"%.2f", orderItem.Unit_Price];
                NSString *BlindSalesOrderItemorderItemQuantity_in_Set=[NSString stringWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Quantity_in_Set];
                
                
                float total = [orderItemUnit_Price floatValue]*[BlindSalesOrderItemorderItemQuantity_in_Set floatValue]*orderItem.Quantity;
                
                
                if(orderItem.Discount_Amount==0)
                {
                    discCheck = orderItem.Discount_Percent;
                    totalDisc = (discCheck/100)*total;
                    discountStr=[NSString stringWithFormat:@"%.2f %%",discCheck];
                }
                else if(orderItem.Discount_Percent==0)
                {
                    discCheck = orderItem.Discount_Amount;
                    totalDisc = discCheck;
                    discountStr=[NSString stringWithFormat:@"RM %.2f",discCheck];
                }
                    total = total - totalDisc;
                
                
                
                NSString* quantitySetStr =[NSString stringWithFormat:@"%.2f", orderItem.Quantity];
                cell.itemQuantityLabel.text = quantitySetStr;
                cell.itemWidthDrop.text = [NSString stringWithFormat:@"%.2fx%.2f", ((BlindSalesOrderItem*)orderItem).Width,((BlindSalesOrderItem*)orderItem).Height];
                cell.itemSqrFeet.text = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Height*((BlindSalesOrderItem*)orderItem).Width];
                
                 
                //cell.itemArea.text = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Height*((BlindSalesOrderItem*)orderItem).Width];
                cell.itemArea.text = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Quantity_in_Set];
                
                NSString* totalStr=[NSString stringWithFormat:@"%.2f",total];
                cell.totalPriceLabel.text=totalStr;
                
                cell.orderControl.text=((BlindSalesOrderItem*)orderItem).Control;
                cell.orderColor.text=((BlindSalesOrderItem*)orderItem).Color;
                
               
                cell.orderDiscount.text=discountStr;
                
                NSString* priceStr=[NSString stringWithFormat:@"%.2f",orderItem.Unit_Price];
                cell.orderPriceLabel.text=priceStr;
            }
            else 
            {
                NSString* discountStr;
                float total = orderItem.Unit_Price*((BlindSalesOrderItem*)orderItem).Quantity;
                if(orderItem.Discount_Amount==0)
                {
                    discCheck = orderItem.Discount_Percent;
                    totalDisc = (discCheck/100)*total;
                    
                    discountStr=[NSString stringWithFormat:@"%.2f %%",discCheck];
                }
                else if (orderItem.Discount_Percent==0)
                {
                    discCheck = orderItem.Discount_Amount;
                    totalDisc = discCheck;
                    discountStr=[NSString stringWithFormat:@"RM %.2f",discCheck];
                }
                total = total - totalDisc;
                
                NSString* quantitySetStr =[NSString stringWithFormat:@"%.2f", orderItem.Quantity];
                cell.itemQuantityLabel.text = quantitySetStr;
                //cell.itemWidthDrop.text = [NSString stringWithFormat:@"%.2fx%.2f", ((BlindSalesOrderItem*)orderItem).Height,((BlindSalesOrderItem*)orderItem).Width];
                //cell.itemSqrFeet.text = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Height*((BlindSalesOrderItem*)orderItem).Width];
                
                //cell.itemArea.text = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Quantity_in_Set];
                
                NSString* totalStr=[NSString stringWithFormat:@"%.2f",total];
                cell.totalPriceLabel.text=totalStr;
                
                //cell.orderControl.text=((BlindSalesOrderItem*)orderItem).Control;
                //cell.orderColor.text=((BlindSalesOrderItem*)orderItem).Color;
                
                
                cell.orderDiscount.text=discountStr;
                
                NSString* priceStr=[NSString stringWithFormat:@"%.2f",orderItem.Unit_Price];
                cell.orderPriceLabel.text=priceStr;
            }
            
            
        }
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [scrollDelegate scrollToItem:indexPath];
    [scrollDelegate focusOnCell:indexPath];
}
- (void)dealloc
{
    [scrollDelegate release];
    [super dealloc];
}

@end
