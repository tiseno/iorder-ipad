//
//  UITableViewWithLoadingNotification.m
//  KDS
//
//  Created by Tiseno on 1/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "UITableViewWithLoadingNotification.h"


@implementation UITableViewWithLoadingNotification
@synthesize loadingDelegate;

-(void)dealloc
{
    [loadingDelegate release];
    [super dealloc];
}
-(void)reloadData
{
    [super reloadData];
    [loadingDelegate loadDataFinished];
}
@end
 