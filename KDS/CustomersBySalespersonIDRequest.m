//
//  CustomersBySalespersonIDRequest.m
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/6/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "CustomersBySalespersonIDRequest.h"


@implementation CustomersBySalespersonIDRequest
@synthesize salesPersonID;
-(id)initWithSalesPersonID:(NSString*)isalesPersonID
{
    self=[super init];
    if(self)
    {
        self.SOAPAction=@"GetCustomersBySalespersonID";
        self.requestType=WebserviceRequest;
        self.salesPersonID=isalesPersonID;
    }
    return self;
}
-(void)dealloc
{
    [salesPersonID release];
    [super dealloc];
}
-(NSString*) generateHTTPPostMessage
{
    NSString *xmlStr=[NSString stringWithFormat:@"salesPersonID=%@",self.salesPersonID];
	return xmlStr;
}
@end
