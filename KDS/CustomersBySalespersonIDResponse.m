//
//  CustomersBySalespersonIDResponse.m
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/6/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "CustomersBySalespersonIDResponse.h"


@implementation CustomersBySalespersonIDResponse
@synthesize customers;
-(void)dealloc
{
    [customers release];
    [super dealloc];
}
@end
