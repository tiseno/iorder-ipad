//
//  KDSDataSalesOrder.h
//  KDS
//
//  Created by Tiseno on 12/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KDSDataLayerBase.h"
#import "SalesOrder.h"
#import "BlindSalesOrder.h"
#import "SalesPerson.h"
#import "LastSellingPrice.h"
#import "KDSDataSalesOrderItem.h"
#import "KDSDateOrderPackage.h"
#import "OrderPackaging.h"
#import "KDSAppDelegate.h"

@interface KDSDataSalesOrder : KDSDataLayerBase {
    
    BOOL runBatch;
}
@property (nonatomic) BOOL runBatch;

-(NSString*)generateInsertQueryForSaleOrder:(SalesOrder*)salesOreder AndSalesperson:(SalesPerson*)salesPerson;
-(void)insertOrderItems:(NSArray*)orderItemArr forSalesOrderID:(int)salesOrderID;
-(NSMutableArray*)selectSalesOrdersforSalesPerson:(SalesPerson*)salesPerson IsUploaded:(BOOL)isUploaded IsDeleted:(BOOL)isDeleted;
-(DataBaseInsertionResult)insertSalesOrder:(SalesOrder*)tSalesOrder forSalesperson:(SalesPerson*)salesPerson;
-(NSMutableArray*)selectSalesOrderForCustomer:(Customer*)customer IsDeleted:(BOOL)isDeleted;
-(DataBaseInsertionResult)insertLastSellingPrice:(LastSellingPrice*)lastSellingPrice ForCustomer:(Customer*)customer AndSalePerson:(SalesPerson*)salesPerson andItem:(Item*)item;
-(DataBaseUpdateResult)markSalesOrderAsUploaded:(SalesOrder*)salesOrder;
-(DataBaseDeletionResult)deleteSaleOrder:(SalesOrder*)saleOrder;
-(DataBaseUpdateResult)updateSaleOrderIsDeleted:(SalesOrder*)saleOrder;
-(DataBaseUpdateResult)updateSaleOrder:(SalesOrder*)saleOrder ForSaleOrderId:(SalesPerson*)salePerson;

//-(DataBaseUpdateResult)updateCustomerRemark:(Customer*)tCustomer;
-(NSMutableArray*)selectOneSalesOrdersforSalesPerson:(SalesPerson*)salesPerson IsUploaded:(BOOL)isUploaded IsDeleted:(BOOL)isDeleted IsSalesOrderID:(SalesOrder*)issalesOrder;
@property (nonatomic,retain) NSArray *orderPackagesArr;

-(DataBaseUpdateResult)updateSaleOrderPackageID:(SalesOrder*)saleOrder;
-(DataBaseUpdateResult)updateOrderPackageArray:(NSArray*)iorderpackage;
@end
