//
//  AllItemDetailsResponse.m
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/6/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "AllItemDetailsResponse.h"


@implementation AllItemDetailsResponse
@synthesize items;
-(void)dealloc
{
    [items release];
    [super dealloc];
}
@end
