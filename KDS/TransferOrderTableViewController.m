//
//  TransferOrderTableViewController.m
//  KDS
//
//  Created by Jermin Bazazian on 5/28/12.
//  Copyright (c) 2012 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "TransferOrderTableViewController.h"

//@interface TransferOrderTableViewController ()

//@end

@implementation TransferOrderTableViewController
@synthesize orderListViewControllersystm, newtransferSalesOrderArr, newtransferOrdersNotTransferedArr;

- (id)initWithStyle:(UITableViewStyle)style 
{
    self = [super initWithStyle:style];
    if (self) {
        [self initializeTableData];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.newtransferSalesOrderArr!=nil)
    {
        return [self.newtransferSalesOrderArr count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    SalesOrder* salesOrder=[self.newtransferSalesOrderArr objectAtIndex:indexPath.row];
    OrderListTransferedCellViewController* cell = [[OrderListTransferedCellViewController alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    //if(cell == nil)
    //{ }
        cell.delegate=self.orderListViewControllersystm;
    cell.salesOrder=salesOrder;
        cell.lineColor = [UIColor blackColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.customerNameLabel.text = salesOrder.customer.Customer_Name;
        NSString* grandPrice = [NSString stringWithFormat:@"RM %.2f", salesOrder.grand_Price];
        cell.priceLabel.text = grandPrice;
   
    
    if(indexPath.row == 0)
        cell.topCell = YES;
    else
        cell.topCell = NO;
    
    return cell;
    
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

-(void)initializeTableData
{
    /*KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    KDSDataSalesOrder *dataSalesOrder;
    if(appDelegate.salesOrder.Type==SalesOrderMain)
    {
        dataSalesOrder = [[KDSDataSalesOrder alloc] init];
    }
    else if(appDelegate.salesOrder.Type==SalesOrderBlind)
    {
        dataSalesOrder = [[KDSDataBlindSalesOrder alloc] init];
    }
    NSArray* tNewSalesOrderArr=[dataSalesOrder selectSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:NO IsDeleted:NO];
    self.newSalesOrderArr=tNewSalesOrderArr;
    [dataSalesOrder release];
    */
    KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    KDSDataSalesOrder *dataSalesOrder=[[KDSDataSalesOrder alloc] init];
    NSArray* tNewSalesOrderArr=[dataSalesOrder selectSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:NO IsDeleted:NO];
    self.newtransferSalesOrderArr=tNewSalesOrderArr;
    [dataSalesOrder release];
}

-(void)dealloc
{
    [newtransferSalesOrderArr release];
    [orderListViewControllersystm release];
    [super dealloc];
}

@end
