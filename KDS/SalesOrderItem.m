//
//  SalesOrderItem.m
//  KDS
//
//  Created by Jermin Bazazian on 12/8/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "SalesOrderItem.h"


@implementation SalesOrderItem
@synthesize item,Quantity=_Quantity,Unit_Price=_Unit_Price,Order_UOM=_Order_UOM,Discount_Percent=_Discount_Percent,Discount_Amount=_Discount_Amount,Comment=_Comment;
@synthesize TotalPrice=_TotalPrice,blinditems;
@synthesize SpecialItemDescription=_SpecialItemDescription;
-(id)init
{
    self=[super init];
    if(self)
    { 
        
        _Discount_Percent=-1.0f;
        _Discount_Amount=-1.0f;
        
        
    }
    return self;
}
-(void)dealloc
{
    [_SpecialItemDescription release];
    [blinditems release];
    [item release];
    [_Comment release];
    [_Order_UOM release];
    [super dealloc];
}
-(void)addInheritanceDetails:(GDataXMLElement*)salesOrderItemNode
{
    
}

-(void)addInheritancePDFDetails:(GDataXMLElement*)salesOrderItemNode
{
    
}
@end
