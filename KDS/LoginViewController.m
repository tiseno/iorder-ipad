//
//  LoginViewController.m
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"

@implementation LoginViewController

@synthesize userNameTextView, passwordTextView, navController,loadingView,superViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(BOOL)NetworkStatus
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus==NotReachable);
}
- (void)dealloc
{
    [navController release];
    [userNameTextView release];
    [passwordTextView release];
    [loadingView release];
    [superViewController release];
    [super dealloc];
}

-(IBAction)loginButtonTapped
{
    if(![userNameTextView.text isEqualToString:@""] && ![passwordTextView.text isEqualToString:@""])
    {
        KDSDataSalesPerson *dataSalePerson=[[KDSDataSalesPerson alloc] init];
        SalesPerson* salePerson=[dataSalePerson selectSalesPersonWithUsername:userNameTextView.text Password:passwordTextView.text];
        [dataSalePerson release];
        if(salePerson!=nil && ![salePerson.SalesPerson_Code isEqualToString:@""])
        {
            KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
            [appDelegate loginWithSalePerson:salePerson];
            [self.superViewController loadMenu];
            /*MenuViewController *menuViewController=[[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
            menuViewController.title=@"Felton";
            UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:menuViewController];
            tnavController.view.frame=CGRectMake(0, 0, 1024, 748);
            [menuViewController release];
            self.navController=tnavController;
            [navController release];
            for(UIView *subview in self.view.subviews)
            {
                [subview removeFromSuperview];
            }
            [self.view addSubview:self.navController.view];*/
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Wrong Credentials" message:@"Wrong Username and/or password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Wrong Credentials" message:@"Username and/or password is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}
-(IBAction)syncButtonTapped
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        BOOL isLoadingDisplayed=NO;
        for(UIView *subView in self.view.subviews)
        {
            if ([subView class]==[LoadingView class]) {
                isLoadingDisplayed=YES;
            }
        }
            if(!isLoadingDisplayed)
            {
                UIView *selfView=self.view;
                LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
                self.loadingView=temploadingView;
            }
        NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
        [networkHandler setDelegate:self];
        AllSalesPeopleRequest *request=[[AllSalesPeopleRequest alloc] init];
        [networkHandler request:request];
        [request release];
        [networkHandler release];
    }    
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self loginButtonTapped];
    return YES;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    userNameTextView.returnKeyType = UIReturnKeyGo;
    passwordTextView.returnKeyType = UIReturnKeyGo;
    /**/
    userNameTextView.text = @"bill";
    passwordTextView.text = @"bill123";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    heightOfEditedView = textField.frame.size.height;
    heightOffset = textField.frame.origin.y+10;
    
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOfEditedView+heightOffset), self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    if(textField.tag == kPriceTextFeield)
    {
        [textField selectAll:nil];
    }
}

-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight) {
        return YES;
    }
    return NO;
}
-(void)handleRecievedResponseMessage:(XMLResponse*)responseMessage
{
    if([responseMessage isKindOfClass:[AllSalesPeopleResponse class]])
    {
        KDSDataSalesPerson *dataSalePerson=[[KDSDataSalesPerson alloc] init];
        [dataSalePerson insertSalesPersonArray:((AllSalesPeopleResponse*)responseMessage).salesPeople];
        [dataSalePerson release];
        [self.loadingView removeView];
    }
}
@end
