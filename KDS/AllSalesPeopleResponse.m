//
//  AllSalesPeopleResponse.m
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/6/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "AllSalesPeopleResponse.h"


@implementation AllSalesPeopleResponse
@synthesize salesPeople;
-(void)dealloc
{
    [salesPeople release];
    [super dealloc];
}
@end
