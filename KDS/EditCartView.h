//
//  EditCartView.h
//  KDS
//
//  Created by Tiseno Mac 2 on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageViewWithImageFromURL.h"

@interface EditCartView : UIViewController{
    
}

@property (nonatomic, retain) UITextField *quantityTextField;
@property (nonatomic, retain) UITextField *priceTextField;
@property (nonatomic, retain) UITextField *discountTextField;
@property (nonatomic, retain) UITextView *descriptionTextView;

@property (nonatomic, retain) UIButton *deleteButton;
@property (nonatomic, retain) UIButton *changeDiscountTypeButton;

@property (nonatomic, retain) UILabel *quantityLabel;
@property (nonatomic, retain) UILabel *productNameLabel;
@property (nonatomic, retain) UILabel *descriptionLabel;
@property (nonatomic, retain) UILabel *priceLabel;
@property (nonatomic, retain) UILabel *discountLabel;
@property (nonatomic, retain) UILabel *discountTypeLabel;

@property (nonatomic, retain) UIImageViewWithImageFromURL *productImageView;
@end
