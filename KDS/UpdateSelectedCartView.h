//
//  UpdateSelectedCartView.h
//  KDS
//
//  Created by Tiseno Mac 2 on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import <Foundation/Foundation.h>


@protocol UpdateSelectedCartView <NSObject>
-(void)UpdateScrollToItem:(NSIndexPath *)indexPath;
//-(void)focusOnCell:(NSIndexPath*)indexPath;
@end
