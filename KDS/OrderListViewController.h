//
//  OrderListViewController.h
//  KDS
//
//  Created by Tiseno on 11/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "NewOrderTableViewController.h"
#import "TransferedOrderTableViewController.h"
#import "NewOrderTableEditDelegate.h"
#import "TransferedOrderDeleteDelegate.h"
#import "YourCartViewController.h"
#import "SalesOrder.h"
#import "SalesOrderType.h"
#import "NetworkHandler.h"
#import "TransferOrderPdfRequest.h"
#import "TransferOrderPDFResponse.h"
#import "XMLResponse.h"
#import <MessageUI/MessageUI.h>
#import "LoadingView.h"
#import "Reachability.h"
#import "DeletedCartViewController.h"

#define kConfirmingAlertTag 0
#define kConfirmedAlertTag 1
@class UIDatePicker;
@interface OrderListViewController : UIViewController <NetworkHandlerDelegate, NewOrderTableEditDelegate, TransferedOrderDeleteDelegate, UIAlertViewDelegate,MFMailComposeViewControllerDelegate>{
    
    ///NewOrderTableViewController *newOrderTableViewController;
    TransferedOrderTableViewController *transferedOrderViewController;
    
    UITableView *newOrderTabelView;
    UITableView *transferedOrderTableView;
    
    UIView *newOrderTableViewHeader;
    UIView *transferedOrderTableViewHeader;
    
    UIDatePicker* datePicker;
    BOOL isPickerHidden;
    UIButton* cancelButton;
    
    SalesOrder* salesOrder;
    
    NSArray* salesOrderArr;
    LoadingView *loadingView;
    
} 

@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic) BOOL isPickerHidden;
@property (nonatomic, retain) IBOutlet UIButton* cancelButton;
@property (nonatomic, retain) SalesOrder* salesOrder;
@property (nonatomic, retain) UIView *newOrderTableViewHeader;
@property (nonatomic, retain) UIView *transferedOrderTableViewHeader;
@property (nonatomic, retain) IBOutlet UITableView *newOrderTabelView;;
@property (nonatomic, retain) IBOutlet UITableView *transferedOrderTableView;
@property (nonatomic, retain) NSArray* salesOrderArr;
@property (nonatomic, retain) IBOutlet UIDatePicker* datePicker;
//@property (nonatomic, retain) NewOrderTableViewController *newOrderTableViewController;
@property (nonatomic, retain) TransferedOrderTableViewController *transferedOrderViewController;
-(void)initializeTableData;
-(void)initializeTableDataR;
@property (retain, nonatomic) IBOutlet UIButton *BtnDeleteOrderByDate;
@end
