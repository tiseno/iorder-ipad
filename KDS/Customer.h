//
//  Customer.h
//  KDS
//
//  Created by Tiseno on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Address.h"


@interface Customer : NSObject {
    
    NSString *_salesperson_Code;
    NSString *_Customer_Id;
    NSString *_Customer_Name;
    NSString *_Contact_Name;
    NSString *_Phone_1;
    NSString *_Phone_2;
    NSString *_Email;
    //NSString *customer_remark;
    
    Address *_Address;
}

@property (nonatomic, retain) NSString *Customer_Id, *Customer_Name, *Contact_Name, *Phone_1, *Phone_2, *Email, *salesperson_Code;
@property (nonatomic, retain) Address *Address;
//@property (nonatomic, retain) NSString *customer_remark;
@end
