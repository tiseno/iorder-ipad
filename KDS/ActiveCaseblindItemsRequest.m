//
//  ActiveCaseblindItemsRequest.m
//  KDS
//
//  Created by Tiseno Mac 2 on 5/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ActiveCaseblindItemsRequest.h"

@implementation ActiveCaseblindItemsRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        self.webserviceURL=@"http://demo.kds.my:8088/KDSBlindsItem/service.asmx";
        self.SOAPAction=@"GetActiveBlindsItem";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{
    return @"";
}
@end
