//
//  Item.h
//  KDS
//
//  Created by Tiseno on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AsyncImageViewDelegate.h"

@interface Item : NSObject<AsyncImageViewDelegate> {
    
    int _Item_id;
    NSString * _Item_No;
    NSString * _Description;
    NSString * _FMT_Item_No;
    NSString * _Category;
    NSString * _Stock_Unit;
    NSString * _Image_Path;
    NSMutableArray * _UOMAndConversion;
    NSMutableArray * _Default_Price;
    NSMutableDictionary* _LastSellingPrices;

    
    float edit_min_Height;
    float edit_min_SqFt;
    float edit_total_SqFt;
}
@property(nonatomic) float edit_min_Height;
@property(nonatomic) float edit_min_SqFt;
@property(nonatomic) float edit_total_SqFt;

@property (nonatomic, retain) NSString *Item_No, *Description, *FMT_Item_No, *Category, *Stock_Unit, *Image_Path;
@property (nonatomic, retain) NSMutableArray *UOMAndConversion;
@property (nonatomic, retain) NSMutableArray *Default_Price;
@property (nonatomic, retain) NSMutableDictionary* LastSellingPrices;
@property (nonatomic) int Item_id;
-(NSArray*)lastSellingPriceForCustomer:(NSString*)customerCode;
-(void)addLastSellingPriceArr:(NSArray*)lastSellingPriceArr forCustomerCode:(NSString*)customerCode;
@end
