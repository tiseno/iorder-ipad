//
//  OrderMenupage.h
//  KDS
//
//  Created by Tiseno Mac 2 on 6/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrdereditViewController.h"
#import "OrderTransferedViewController.h"
#import "KDSDateOrderPackage.h"
#import "OrderPackaging.h"
#import "KDSAppDelegate.h"

@interface OrderMenupage : UIViewController{
    
}

-(IBAction)ordertapped:(id)sender;
-(IBAction)transferredordertapped:(id)sender;
@property (nonatomic, retain) NSArray* tTransferecSalesOrderPackageArr;
@end
