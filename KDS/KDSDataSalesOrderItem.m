//
//  KDSDataSalesOrderItem.m
//  KDS
//
//  Created by Tiseno on 12/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "KDSDataSalesOrderItem.h"


@implementation KDSDataSalesOrderItem 
@synthesize runBatch;
-(id)initWithDataBase:(sqlite3*)dataBase
{
    self=[super init];
    if(self)
    {
        dbKDS=dataBase;
    }
    return self;
}
 
-(NSString*)generateInsertQueryForSaleOrderItem:(SalesOrderItem*)salesOrederItem AndSalesOrderID:(int)salesOrderID
{ 
    NSString *salesOrederItemComment=[salesOrederItem.Comment stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *SpecialItemDescription=[salesOrederItem.SpecialItemDescription stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    
    NSString *item_noo=[salesOrederItem.item.Item_No stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    NSString *descriptiono=[salesOrederItem.item.Description stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
    
    
    NSString *insertSQL = [NSString stringWithFormat:@"insert into SalesOrderItem(salesorder_id,item_no,item_description,quantity,unit_price,order_uom,discount_percent,discount_amount,comment,SpecialItemDesc) values(%d,'%@','%@',%f,%f,'%@',%f,%f,'%@','%@');", salesOrderID,item_noo,descriptiono,salesOrederItem.Quantity,salesOrederItem.Unit_Price,salesOrederItem.Order_UOM,salesOrederItem.Discount_Percent,salesOrederItem.Discount_Amount,salesOrederItemComment,SpecialItemDescription];
    return insertSQL;
}

-(NSArray*)selectOrderItemsForOrder:(SalesOrder*)saleOrder
{  
    NSMutableArray* orderItemArr = nil;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        orderItemArr=[[[NSMutableArray alloc] init] autorelease];
        NSString* selectSQL = [NSString stringWithFormat:@"select SalesOrderItem.quantity,SalesOrderItem.unit_price,SalesOrderItem.order_uom,SalesOrderItem.discount_percent,SalesOrderItem.discount_amount,SalesOrderItem.comment,Item.item_no,Item.description,Item.image_path,Item.category,Item.item_id,Item.min_height,Item.min_sqft,SalesOrderItem.width,SalesOrderItem.height,SalesOrderItem.quantity_in_set,SalesOrderItem.control,SalesOrderItem.color,SalesOrderItem.SpecialItemDesc from SalesOrderItem,Item where SalesOrderItem.item_no=Item.Item_no and SalesOrderItem.salesorder_id=%d",saleOrder.Order_id];
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        { 
            /*---------------*/
            //NSLog(@"hello~");
            SalesOrderItem *salesOrderItem;//=[[SalesOrderItem alloc] init];
            float width=(float)sqlite3_column_double(statement, 13);
            NSString *Categorys=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 9)];
            Item* item;
            if([Categorys isEqualToString:kBlindCategory] || [Categorys isEqualToString:kblindACategoryA])
            { 
                salesOrderItem=[[BlindSalesOrderItem alloc] init];
            
                item = [[BlindItem alloc] init];
                ((BlindItem*)item).min_Height = sqlite3_column_double(statement, 11);
                ((BlindItem*)item).min_SqFt = sqlite3_column_double(statement, 12);
                
                //NSLog(@"get Categorys-->%@",Categorys);
            }else 
            {
                salesOrderItem=[[SalesOrderItem alloc] init];
                item=[[Item alloc] init];
            }
            salesOrderItem.Quantity=(float)sqlite3_column_double(statement, 0);
            salesOrderItem.Unit_Price=(float)sqlite3_column_double(statement, 1);
            NSString *UOMStr=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
            salesOrderItem.Order_UOM = UOMStr;
            salesOrderItem.Discount_Percent=(float)sqlite3_column_double(statement, 3);
            salesOrderItem.Discount_Amount=(float)sqlite3_column_double(statement, 4);
            
            NSString *commentStr=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 5)];
            NSString *commentStrg=[commentStr stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            salesOrderItem.Comment = commentStrg;
            
            
            NSString *Sitemdesc=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 18)];
            NSString *Sitemdescd=[Sitemdesc stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            salesOrderItem.SpecialItemDescription=Sitemdescd;
            
            
            NSString *itemNoStr=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 6)];
            NSString *itemNoStrg=[itemNoStr stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            item.Item_No = itemNoStrg;
            NSString *itemDescription=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 7)];
            NSString *itemDescriptionf=[itemDescription stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
            item.Description = itemDescriptionf;
            NSString *imagePath=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 8)];
            item.Image_Path = imagePath;
            
            
            /*============*/ 
            item.Category=Categorys;           
            NSString *Item_ids=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 10)];
            item.Item_id=[Item_ids intValue];
            //NSLog(@"sql Item_id--->%d",[Item_ids intValue]);
            
            
            
            

             /*======
           BlindItem* blindItems=[[BlindItem alloc]init];
            
            NSString *min_Heights=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 11)];
            //blindItems.min_Height=[min_Heights floatValue];
            //salesOrderItem.blinditems.min_Height=[min_Heights floatValue];
            item.edit_min_Height=[min_Heights floatValue];
            
            NSString *min_SqFts=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 12)];
            blindItems.min_SqFt=[min_SqFts floatValue];
            
                        
            (((BlindSalesOrderItem*)salesOrderItem).blindItems).min_Height=(float)sqlite3_column_double(statement, 11);
            (((BlindSalesOrderItem*)salesOrderItem).blindItems).min_SqFt=(float)sqlite3_column_double(statement, 12);
            
           
            NSLog(@"float height--->%.2f",(float)sqlite3_column_double(statement, 11));
            NSLog(@"get height--->%.2f",item.edit_min_Height);
            NSLog(@"float min_SqFt--->%.2f",(float)sqlite3_column_double(statement, 12));
            NSLog(@"get min_SqFt--->%.2f",blindItems.min_SqFt); ======*/
            /*============*/
            
            
            salesOrderItem.item=item;
            [item release];
            
            //salesOrderItem.blinditems=blindItems;
            //[blindItems release];

            
            if([Categorys isEqualToString:kBlindCategory] || [Categorys isEqualToString:kblindACategoryA])
            {
                ((BlindSalesOrderItem*)salesOrderItem).Width=width;
                ((BlindSalesOrderItem*)salesOrderItem).Height=(float)sqlite3_column_double(statement, 14);
                //NSLog(@"BlindSalesOrderItem float height--->%.2f",((BlindSalesOrderItem*)salesOrderItem).Height);
                
                ((BlindSalesOrderItem*)salesOrderItem).Quantity_in_Set=(float)sqlite3_column_double(statement, 15);
                char* controlCStr=(char*)sqlite3_column_text(statement, 16);
                if(controlCStr)
                { 
                    NSString *contorl=[NSString stringWithUTF8String:(char*)controlCStr];
                    NSString *contorld=[contorl stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                    ((BlindSalesOrderItem*)salesOrderItem).Control=contorld;
                }
                char* colorCStr=(char*)sqlite3_column_text(statement, 17);
                if(colorCStr)
                {
                    NSString *color=[NSString stringWithUTF8String:(char*)colorCStr];
                    NSString *colorddd=[color stringByReplacingOccurrencesOfString:@"tisenocodeapos" withString:@"'"];
                    ((BlindSalesOrderItem*)salesOrderItem).Color=colorddd;
                }
                
                
                
                //((BlindSalesOrderItem*)salesOrderItem)
                
            }
            
            
            
            [orderItemArr addObject:salesOrderItem];
            [salesOrderItem release];
        }
        [self closeConnection];
        
    }
    return  orderItemArr;
}

-(DataBaseInsertionResult)insertSalesOrderItem:(SalesOrderItem*)salesOrderItem ForSalesOrderID:(int)salesOrderID
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *insertSQL=[self generateInsertQueryForSaleOrderItem:salesOrderItem AndSalesOrderID:salesOrderID];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
    
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}
-(DataBaseDeletionResult)deleteFromSalesOrderItemForOrder:(SalesOrder*)salesOrder
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString* deleteSQL=[NSString stringWithFormat:@"delete from SalesOrderItem where salesorder_id=%d;", salesOrder.Order_id];
        sqlite3_stmt *statement;
        const char *insert_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection]; 
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return DataBaseDeletionFailed;
}
@end
