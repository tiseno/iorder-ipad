//
//  ActiveCaseItemsRequest.m
//  KDS
//
//  Created by Tiseno Mac 2 on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ActiveCaseItemsRequest.h"

@implementation ActiveCaseItemsRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        self.webserviceURL=@"http://demo.kds.my:8088/KDSAllActiveItems/service.asmx";
        self.SOAPAction=@"GetActiveItem";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{
    return @"";
}
@end
