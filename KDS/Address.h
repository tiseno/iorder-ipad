//
//  Address.h
//  KDS
//
//  Created by Tiseno on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Address : NSObject {
    
    NSString *_State;
    NSString *_Street_1;
    NSString *_Street_2;
    NSString *_Street_3;
    NSString *_Street_4;
    NSString *_City;
    NSString *_ZipCode;
    NSString *_PostalCode;
    NSString *_Country;
}

@property (nonatomic, retain) NSString *Street_1, *Street_2, *Street_3, *Street_4, *City, *ZipCode, *PostalCode, *Country, *State;
@end
