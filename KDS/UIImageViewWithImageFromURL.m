//
//  UIImageViewWithImageFromURL.m
//  KDS
//
//  Created by Jermin Bazazian on 12/15/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "UIImageViewWithImageFromURL.h"


@implementation UIImageViewWithImageFromURL

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [super dealloc];
}

-(void)handleRecieveImage:(UIImage*)image sender:(AsyncImageView*)asyncImageView
{
    self.image=image;
}
@end
